// WKNavigationDelegate
// println implementation for each method in protocol
//
// IDECodeSnippetCompletionScopes: [ClassImplementation]
// IDECodeSnippetIdentifier: A6B25E40-CD34-489C-961B-2D1960EA9CA8
// IDECodeSnippetLanguage: Xcode.SourceCodeLanguage.Swift
// IDECodeSnippetUserSnippet: 1
// IDECodeSnippetVersion: 2
    // MARK: WKNavigationDelegate
    
    // class X : UIViewController, WKNavigationDelegate { ...
    // override func viewDidLoad() { webView.navigationDelegate = self  ...
    
    // WKNavigation is an empty object used as a token used for tracking the loading progress of a webpage.
    // It is also passed to the navigation delegate methods, to uniquely identify a webpage load from start to finish.
    
    // Decide whether to allow or cancel a navigation.
    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        println("decidePolicyForNavigationAction: \(navigationAction)")
        decisionHandler(.Allow)   // .Cancel|.Allow
    }
    
    // Decide whether to allow or cancel a navigation after its response is known.
    func webView(webView: WKWebView, decidePolicyForNavigationResponse navigationResponse: WKNavigationResponse, decisionHandler: (WKNavigationResponsePolicy) -> Void) {
        println("decidePolicyForNavigationResponse: \(navigationResponse)")
        decisionHandler(.Allow)   // .Cancel|.Allow
    }
    
    // Invoked when a main frame navigation starts.
    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        println("didStartProvisionalNavigation: \(navigation)")
    }
    
    // Invoked when a server redirect is received for the main frame.
    func webView(webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        println("didReceiveServerRedirectForProvisionalNavigation: \(navigation)")
    }
    
    // Invoked when an error occurs while starting to load data for the main frame.
    func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        println("didFailProvisionalNavigation: \(navigation), \(error)")
    }
    
    // Invoked when content starts arriving for the main frame.
    func webView(webView: WKWebView, didCommitNavigation navigation: WKNavigation!) {
        println("didCommitNavigation: \(navigation)")
    }
    
    // Invoked when a main frame navigation completes.
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        println("didFinishNavigation: \(navigation)")
    }
    
    // Invoked when an error occurs during a committed main frame navigation.
    func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError) {
        println("didFailNavigation: \(navigation), \(error)")
    }
    
    // Invoked when the web view needs to respond to an authentication challenge.
    // When disposition is NSURLSessionAuthChallenge.UseCredential, the credential argument is the credential to use, or nil to indicate continuing without a
    // credential.
    func webView(webView: WKWebView, didReceiveAuthenticationChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential!) -> Void) {
        println("didReceiveAuthenticationChallenge: \(challenge)")
        /*
        UseCredential                   // Use the specified credential, which may be nil
        PerformDefaultHandling          // Default handling for the challenge - as if this delegate were not implemented; the credential parameter is ignored.
        CancelAuthenticationChallenge   // The entire request will be canceled; the credential parameter is ignored.
        RejectProtectionSpace           // [Default] This challenge is rejected and the next authentication protection space should be tried;the credential parameter is ignored.
        */
        completionHandler( .UseCredential, challenge.proposedCredential )
        
    }
