// NSCoding Protocol Methods
// Placeholders for NSCoding protocol methods
//
// IDECodeSnippetCompletionScopes: [All]
// IDECodeSnippetIdentifier: 51B86061-C8A9-4AD8-8E39-F2A0FC3270CC
// IDECodeSnippetLanguage: Xcode.SourceCodeLanguage.Objective-C
// IDECodeSnippetUserSnippet: 1
// IDECodeSnippetVersion: 2

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
      return nil;
    }

    <# implementation #>

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  <# implementation #>
}
